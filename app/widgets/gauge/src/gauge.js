'use strict';

angular.module('adf.widget.gauge', ['adf.provider', 'frapontillo.gage'])
  .config(function(dashboardProvider) {
    dashboardProvider
      .widget('gauge', {
        title: 'gauge',
        description: 'Displays a gauge',
        templateUrl: '{widgetsPath}/gauge/src/view.html',
        controller: 'gaugeController',
        controllerAs: 'gauge',
        edit: {
          templateUrl: '{widgetsPath}/gauge/src/edit.html'
        }
      });
  })
  .controller('gaugeController', function($scope, config, $document) {
    $scope.config = config;
  });
