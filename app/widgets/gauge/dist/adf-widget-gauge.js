(function(window, undefined) {'use strict';


angular.module('adf.widget.gauge', ['adf.provider', 'frapontillo.gage'])
  .config(["dashboardProvider", function(dashboardProvider) {
    dashboardProvider
      .widget('gauge', {
        title: 'gauge',
        description: 'Displays a gauge',
        templateUrl: '{widgetsPath}/gauge/src/view.html',
        controller: 'gaugeController',
        controllerAs: 'gauge',
        edit: {
          templateUrl: '{widgetsPath}/gauge/src/edit.html'
        }
      });
  }])
  .controller('gaugeController', ["$scope", "config", "$document", function($scope, config, $document) {
    $scope.config = config;
  }]);

angular.module("adf.widget.gauge").run(["$templateCache", function($templateCache) {$templateCache.put("{widgetsPath}/gauge/src/edit.html","<form role=form><div class=form-group><label for=time>Type</label><select class=form-control ng-model=config.type><option value=bar>bar</option><option value=line>line</option><option value=line-dotted>line-dotted</option></select></div><div class=form-group><label for=date>Data</label> <input type=text class=form-control id=date ng-model=config.data></div><div class=form-group><label for=date>Options</label> <input type=text class=form-control id=date ng-model=config.opts></div></form>");
$templateCache.put("{widgetsPath}/gauge/src/view.html","<div justgage title={{config.title}} title-font-color={{config.titleFontColor}} value={{config.value}} value-font-color={{config.valueFontColor}} width={{config.width}} height={{config.height}} relative-gauge-size={{config.relativeGaugeSize}} value-min-font-size={{config.valueMinFontSize}} title-min-font-size={{config.titleMinFontSize}} label-min-font-size={{config.labelMinFontSize}} min-label-min-font-size={{config.minLabelMinFontSize}} maxlabelminfontsize={{config.maxLabelMinFontSize}} min={{config.min}} max={{config.max}} hide-min-max={{config.hideMinMax}} hide-value={{config.hideValue}} hide-inner-shadow={{config.hideInnerShadow}} gauge-width-scale={{config.gaugeWidthScale}} gauge-color={{config.gaugeColor}} show-inner-shadow={{config.showInnerShadow}} shadow-opacity={{config.shadowOpacity}} shadow-size={{config.shadowSize}} shadow-vertical-offset={{config.shadowVerticalOffset}} level-colors={{config.levelColors}} custom-sectors={{config.customSectors}} no-gradient={{config.noGradient}} label={{config.label}} label-font-color={{config.labelFontColor}} start-animation-time={{config.startAnimationTime}} start-animation-type={{config.startAnimationType}} refresh-animation-time={{config.refreshAnimationTime}} refresh-animation-type={{config.refreshAnimationType}} donut={{config.donut}} donut-start-angle={{config.donutStartAngle}} counter={{config.counter}} decimals={{config.decimals}} symbol={{config.symbol}} format-number={{config.formatNumber}} human-friendly={{config.humanFriendly}} human-friendly-decimal={{config.humanFriendlyDecimal}} text-renderer=textRenderer()></div>");}]);})(window);