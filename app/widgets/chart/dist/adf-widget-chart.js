(function(window, undefined) {'use strict';


angular.module('adf.widget.chart', ['ngAnimate', 'adf.provider', 'nvd3'])
  .config(["dashboardProvider", function(dashboardProvider) {
    dashboardProvider
      .widget('chart', {
        title: 'Chart',
        description: 'Displays a chart',
        templateUrl: '{widgetsPath}/chart/src/view.html',
        controller: 'chartController',
        controllerAs: 'chart',
        edit: {
          templateUrl: '{widgetsPath}/chart/src/edit.html'
        }
      });
  }])
  .controller('chartController', ["$scope", "$interval", "config", function($scope, $interval, config) {
    if (config.drillDownEnabled && config.options.chart.type == 'pieChart') {
      $scope.drillingDown = false;
      $scope.goBack = function() {
        $scope.drillingDown = false;
      };
      config.options.chart.pie = {
        dispatch: {
          elementClick: function(e) {
            console.log('click ', e);
            $scope.drillingDown = true;

            config.drillDownData = config.data[e.index].drillDownData;
            $scope.$apply();
          }
        }
      };
    }
  }]);

angular.module("adf.widget.chart").run(["$templateCache", function($templateCache) {$templateCache.put("{widgetsPath}/chart/src/edit.html","<form role=form><div class=form-group><label for=time>Type</label><select class=form-control ng-model=config.type><option value=bar>bar</option><option value=line>line</option><option value=line-dotted>line-dotted</option></select></div><div class=form-group><label for=date>Data</label> <input type=text class=form-control id=date ng-model=config.data></div><div class=form-group><label for=date>Options</label> <input type=text class=form-control id=date ng-model=config.opts></div></form>");
$templateCache.put("{widgetsPath}/chart/src/view.html","<div ng-if=!drillingDown class=animate-if><nvd3 options=config.options data=config.data config=\"{refreshDataOnly: true}\" api=config.api events=config.events></nvd3></div><div ng-if=drillingDown class=animate-if><button class=\"btn btn-primary\" ng-click=goBack()>Go Back</button><nvd3 options=config.drillDownOptions data=config.drillDownData></nvd3><p ng-if=!config.drillDownData>No drill-down data.</p></div>");}]);})(window);