'use strict';

angular.module('adf.widget.chart', ['ngAnimate', 'adf.provider', 'nvd3'])
  .config(function(dashboardProvider) {
    dashboardProvider
      .widget('chart', {
        title: 'Chart',
        description: 'Displays a chart',
        templateUrl: '{widgetsPath}/chart/src/view.html',
        controller: 'chartController',
        controllerAs: 'chart',
        edit: {
          templateUrl: '{widgetsPath}/chart/src/edit.html'
        }
      });
  })
  .controller('chartController', function($scope, $interval, config) {
    if (config.drillDownEnabled && config.options.chart.type == 'pieChart') {
      $scope.drillingDown = false;
      $scope.goBack = function() {
        $scope.drillingDown = false;
      };
      config.options.chart.pie = {
        dispatch: {
          elementClick: function(e) {
            console.log('click ', e);
            $scope.drillingDown = true;

            config.drillDownData = config.data[e.index].drillDownData;
            $scope.$apply();
          }
        }
      };
    }
  });
