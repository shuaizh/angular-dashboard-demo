(function(window, undefined) {'use strict';


angular.module('adf.widget.grid', ['adf.provider', 'ui.grid', 'ui.grid.selection'])
  .config(["dashboardProvider", function(dashboardProvider) {
    dashboardProvider
      .widget('grid', {
        title: 'grid',
        description: 'Displays a grid',
        templateUrl: '{widgetsPath}/grid/src/view.html',
        controller: 'gridController',
        controllerAs: 'grid',
        edit: {
          templateUrl: '{widgetsPath}/grid/src/edit.html'
        }
      });
  }])
  .controller('gridController', ["$scope", "config", "$document", function($scope, config, $document) {
    $scope.gridOptions = config.gridOptions;
  }]);

angular.module("adf.widget.grid").run(["$templateCache", function($templateCache) {$templateCache.put("{widgetsPath}/grid/src/edit.html","<form role=form><div class=form-group><label for=time>Type</label><select class=form-control ng-model=config.type><option value=bar>bar</option><option value=line>line</option><option value=line-dotted>line-dotted</option></select></div><div class=form-group><label for=date>Data</label> <input type=text class=form-control id=date ng-model=config.data></div><div class=form-group><label for=date>Options</label> <input type=text class=form-control id=date ng-model=config.opts></div></form>");
$templateCache.put("{widgetsPath}/grid/src/view.html","<div ui-grid=gridOptions ui-grid-selection class=myGrid></div>");}]);})(window);