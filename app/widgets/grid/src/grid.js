'use strict';

angular.module('adf.widget.grid', ['adf.provider', 'ui.grid', 'ui.grid.selection'])
  .config(function(dashboardProvider) {
    dashboardProvider
      .widget('grid', {
        title: 'grid',
        description: 'Displays a grid',
        templateUrl: '{widgetsPath}/grid/src/view.html',
        controller: 'gridController',
        controllerAs: 'grid',
        edit: {
          templateUrl: '{widgetsPath}/grid/src/edit.html'
        }
      });
  })
  .controller('gridController', function($scope, config, $document) {
    $scope.gridOptions = config.gridOptions;
  });
