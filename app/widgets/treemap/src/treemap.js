'use strict';

angular.module('adf.widget.treemap', ['adf.provider'])
  .config(function(dashboardProvider) {
    dashboardProvider
      .widget('treemap', {
        title: 'treemap',
        description: 'Displays a treemap',
        templateUrl: '{widgetsPath}/treemap/src/view.html',
        controller: 'treemapController',
        controllerAs: 'treemap',
        edit: {
          templateUrl: '{widgetsPath}/treemap/src/edit.html'
        }
      });
  })
  .directive('treemap', function() {
    return {
      restrict: 'E',
      replace: true,
      // transclude: false,
      compile: function(element, attrs) {
        // generate a unique id
        var uniqid = Date.now();
        var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
        var id = randLetter + Date.now();

        var html = '<div id=' + id + ' class="viz"></div>';
        var newElem = $(html);
        element.replaceWith(newElem);

        return function(scope, element, attrs, controller) {
          var container = d3.select('#' + id);

          function drawTreemap() {
            container.html('');
            var visualization = d3plus.viz()
              .container(container) // container DIV to hold the visualization
              .data(scope.data) // data to use with the visualization
              .type("tree_map") // visualization type
              .labels({
                "align": "left",
                "valign": "top"
              })
              .id("name") // key for which our data is unique on
              .size("value") // sizing of blocks
              .draw(); // finally, draw the visualization!
          }



          // redraw treemap when the width of its parent container changes
          var timeOut = null;
          var lastParentContainerWidth = $('#' + id).parent().parent().width();
          window.onresize = function() {
            var container = $('#' + id);
            var currentParentContainerWidth = container.parent().parent().width();
            if (currentParentContainerWidth != lastParentContainerWidth) {
              if (timeOut != null) {
                clearTimeout(timeOut);
              }

              timeOut = setTimeout(function() {
                console.debug('redrawing treemap...');
                container.removeAttr('style');
                drawTreemap();
              }, 500);

              lastParentContainerWidth = currentParentContainerWidth;
            }
          };

          // redraw treemap when data changed
          scope.$watch('data', function(newValue, oldValue) {
            if (newValue) {
              drawTreemap();
            }
          }, true);
        }
      }
    }
  })
  .controller('treemapController', function($scope, config, $document) {
    $scope.data = config.data;
  });
