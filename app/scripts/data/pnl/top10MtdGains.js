var top10MtdGainsOptions = {
  chart: {
    type: 'multiBarHorizontalChart',
    height: 500,
    x: function(d) {
      return d.label;
    },
    y: function(d) {
      return d.value;
    },
    showControls: false,
    showValues: true,
    showLegend:false,
    transitionDuration: 500,
    xAxis: {
      showMaxMin: false
    },
    yAxis: {
      axisLabel: 'MTD PnL',
      tickFormat: function(d) {
        return d3.format(',')(d);
      }
    },
    valueFormat: function(d) {
      return d3.format(',')(d);
    },
  }
};

var top10MtdGainsData = [{
  "key": "Top 10 MTD Gains",
  "color": "#1f77b4",
  "values": [{
    "label": "HEDGE",
    "value": 3049576
  }, {
    "label": "KO INV",
    "value": 1899078
  }, {
    "label": "AGN RA",
    "value": 1397303
  }, {
    "label": "CBST RA",
    "value": 1204062
  }, {
    "label": "STZ SS",
    "value": 899048
  }, {
    "label": "GFIG RA",
    "value": 604823
  }, {
    "label": "TSLA SHORT",
    "value": 503124
  }, {
    "label": "WPZ RA",
    "value": 447624
  }, {
    "label": "RVBD RA",
    "value": 390167
  }, {
    "label": "SWY RA",
    "value": 260060
  }]
}];
