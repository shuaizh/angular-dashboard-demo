var buildPnlByBookChartOptions = function(scope) {
  return {
    chart: {
      type: 'pieChart',
      height: 500,
      x: function(d) {
        return d.book;
      },
      y: function(d) {
        return d.mtdPnl;
      },
      showLabels: true,
      transitionDuration: 500,
      labelThreshold: 0.01,
      legend: {
        margin: {
          top: 5,
          right: 35,
          bottom: 5,
          left: 0
        }
      }
    },
    title: {
      "enable": true,
      "text": 'PnL By Book'
    },
    subtitle: {
      "enable": true,
      "text": 'Total: ($50.6) million'
    }
  }
};

var pnlByBookData = [{
  book: 'ARBITRAGE',
  mtdPnl: 8646111,
  drillDownData: [{
    key: 'RISK ARB',
    values: [{
      mtd: 1025474,
      date: '01/06/2015'
    }, {
      mtd: 1397303,
      date: '01/07/2015'
    }, {
      mtd: 466169,
      date: '01/08/2015'
    }, {
      mtd: 532487,
      date: '01/09/2015'
    }, {
      mtd: 325312,
      date: '01/10/2015'
    }, {
      mtd: 532487,
      date: '01/11/2015'
    }, {
      mtd: 253248,
      date: '01/12/2015'
    }, {
      mtd: 678481,
      date: '01/13/2015'
    }, {
      mtd: 63547,
      date: '01/14/2015'
    }, {
      mtd: 532871,
      date: '01/15/2015'
    }]
  }, {
    key: 'SPECIAL SITUATIONS',
    values: [{
      mtd: 123121,
      date: '01/06/2015'
    }, {
      mtd: 324122,
      date: '01/07/2015'
    }, {
      mtd: 661629,
      date: '01/08/2015'
    }, {
      mtd: 1232123,
      date: '01/09/2015'
    }, {
      mtd: 102078,
      date: '01/10/2015'
    }, {
      mtd: 1436456,
      date: '01/11/2015'
    }, {
      mtd: 1354544,
      date: '01/12/2015'
    }, {
      mtd: 95465,
      date: '01/13/2015'
    }, {
      mtd: 674522,
      date: '01/14/2015'
    }, {
      mtd: 321343,
      date: '01/15/2015'
    }]
  }, {
    key: 'STUB ARB',
    values: [{
      mtd: 162008,
      date: '01/06/2015'
    }, {
      mtd: 152770,
      date: '01/07/2015'
    }, {
      mtd: 183546,
      date: '01/08/2015'
    }, {
      mtd: 160002,
      date: '01/09/2015'
    }, {
      mtd: 132078,
      date: '01/10/2015'
    }, {
      mtd: 162278,
      date: '01/11/2015'
    }, {
      mtd: 142384,
      date: '01/12/2015'
    }, {
      mtd: 156879,
      date: '01/13/2015'
    }, {
      mtd: 172954,
      date: '01/14/2015'
    }, {
      mtd: 165338,
      date: '01/15/2015'
    }]
  }]
}, {
  book: 'CREDIT',
  mtdPnl: 1552744
}, {
  book: 'INVESTMENT',
  mtdPnl: 41198636
}, {
  book: 'SHORT',
  mtdPnl: 3646766
}, {
  book: 'TRADING',
  mtdPnl: 2892975
}];
