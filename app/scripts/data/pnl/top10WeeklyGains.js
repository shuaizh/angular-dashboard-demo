var top10WeeklyGainsOptions = {
  chart: {
    type: 'discreteBarChart',
    height: 500,
    margin: {
      left:80,

      bottom: 80,

    },
    x: function(d) {
      return d.label;
    },
    y: function(d) {
      return d.value;
    },
    showValues: true,
    valueFormat: function(d) {
      return d3.format(',')(d);
    },
    transitionDuration: 500,
    xAxis: {

    },
    yAxis: {
      axisLabel: 'Weekly PnL',
      axisLabelDistance:20
    }
  }
};

var top10WeeklyGainsData = [{
  key: "Cumulative Return",
  values: [{
    "label": "RBS LN INV",
    "value": 2558615
  }, {
    "label": "SAB LN INV",
    "value": 2531553
  }, {
    "label": "SBH INV",
    "value": 1500000
  }, {
    "label": "KO INV",
    "value": 1235642
  }, {
    "label": "AGN RA",
    "value": 856885
  }, {
    "label": "MOEX RM INV",
    "value": 705870
  }, {
    "label": "GFIG RA",
    "value": 604618
  }, {
    "label": "UHAL INV",
    "value": 501642
  }, {
    "label": "CBST RA INV",
    "value": 468564
  }, {
    "label": "TITIM HY",
    "value": 342882
  }]
}];
