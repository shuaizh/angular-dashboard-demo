var top10WeeklyLossesOptions = {
  chart: {
    type: 'discreteBarChart',
    height: 500,
    margin: {
      left:80,

      bottom: 80,

    },
    x: function(d) {
      return d.label;
    },
    y: function(d) {
      return d.value;
    },
    showValues: true,
    valueFormat: function(d) {
      return d3.format(',')(d);
    },
    transitionDuration: 500,
    xAxis: {

    },
    yAxis: {
      axisLabel: 'Weekly PnL',
      axisLabelDistance:20
    }
  }
};

var top10WeeklyLossesData = [{
  key: "Cumulative Return",
  values: [{
    "label": "BAC INV",
    "value": 3366604
  }, {
    "label": "C INV",
    "value": 2439000
  }, {
    "label": "COV RA",
    "value": 2111172
  }, {
    "label": "AUXL RA",
    "value": 1953667
  }, {
    "label": "RIO DLC5",
    "value": 1533571
  }, {
    "label": "SAN SM PRIMARY",
    "value": 1344276
  }, {
    "label": "8035 JP RA",
    "value": 1228154
  }, {
    "label": "FDO RA",
    "value": 1081600
  }, {
    "label": "GRF SC7",
    "value": 1017145
  }, {
    "label": "HEDGE",
    "value": 816884
  }]
}]
