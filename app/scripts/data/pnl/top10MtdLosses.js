var top10MtdLossesOptions = {
  chart: {
    type: 'multiBarHorizontalChart',
    height: 500,
    x: function(d) {
      return d.label;
    },
    y: function(d) {
      return d.value;
    },
    showControls: false,
    showValues: true,
    showLegend:false,
    transitionDuration: 500,
    xAxis: {
      showMaxMin: false
    },
    yAxis: {
      axisLabel: 'MTD PnL',
      tickFormat: function(d) {
        return d3.format(',')(d);
      }
    },
    valueFormat: function(d) {
      return d3.format(',')(d);
    },
  }
};

var top10MtdLossesData = [{
  "key": "Top 10 MTD Losses",
  "color": "#d62728",
  "values": [{
    "label": "BAC INV",
    "value": -12236604
  }, {
    "label": "C INV",
    "value": -11426696
  }, {
    "label": "AIG/WS INV",
    "value": -10996207
  }, {
    "label": "FDO RA",
    "value": -4692591
  }, {
    "label": "RBS LN INV",
    "value": -4082591
  }, {
    "label": "DTV RA",
    "value": -2527950
  }, {
    "label": "SAN SM PRIMARY",
    "value": -2369056
  }, {
    "label": "CFG INV",
    "value": -2195732
  }, {
    "label": "MBS MK RA",
    "value": -2149613
  }, {
    "label": "SBH INV",
    "value": -2120000
  }]
}];
