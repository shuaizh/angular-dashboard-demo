var treemapOptions = {
  chart: {
    type: 'treemap',
    width: 1108,
    height: 500
  }
};

var treemapData = {
  name: 'etl jobs',
  children: [{
    name: 'Late',
    size: 1
  }, {
    name: 'Failure',
    size: 1
  }, {
    name: 'In Progress',
    size: 1
  }, {
    name: 'Done',
    children: [{
      name: 'Good job1',
      size: 2
    }, {
      name: 'Good job2',
      size: 3
    }]
  }]
};
