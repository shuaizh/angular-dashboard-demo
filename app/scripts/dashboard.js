'use strict';

angular.module('dashboard', [
    'adf',
    'adf.structures.base',
    'ui.bootstrap',
    'adf.widget.markdown',
    'adf.widget.chart',
    'adf.widget.treemap',
    'adf.widget.grid',
    'adf.widget.gauge',
    'adf.widget.clock',
    'LocalStorageModule'
  ])
  .config(function(dashboardProvider, localStorageServiceProvider, $httpProvider, markdownConverterProvider) {
    dashboardProvider.widgetsPath('widgets/');
    localStorageServiceProvider.setPrefix('adf');
    markdownConverterProvider.config({
      extensions: ['table']
    });

  })
  .run(function($rootScope, localStorageService) {
    var containerClass = localStorageService.get('containerClass');
    $rootScope.containerClass = containerClass ? containerClass : "container";
  })
  .controller('dashboardCtrl', function($scope, $rootScope, $http, $modal, localStorageService) {
    function buildModel(treemapData) {
      model = {
        title: "PnL Dashboard",
        structure: "6-6",
        rows: [{
          columns: [{
            styleClass: "col-md-12",
            widgets: [$scope.pnlByBookPieChart]
          }]
        }, {
          columns: [{
            styleClass: "col-md-6",
            widgets: [{
              fullScreen: true,
              modalSize: 'sm',
              type: "chart",
              config: {
                options: top10WeeklyGainsOptions,
                data: top10WeeklyGainsData
              },
              title: "Top 10 Weekly Gains"
            }]
          }, {
            styleClass: "col-md-6",
            widgets: [{
              fullScreen: true,
              modalSize: 'sm',
              type: "chart",
              config: {
                options: top10WeeklyLossesOptions,
                data: top10WeeklyLossesData
              },
              title: "Top 10 Weekly Losses"
            }]
          }]
        }, {
          columns: [{
            styleClass: "col-md-6",
            widgets: [{
              fullScreen: true,
              modalSize: 'sm',
              type: "chart",
              config: {
                options: top10MtdGainsOptions,
                data: top10MtdGainsData
              },
              title: "Top 10 MTD Gains"
            }]
          }, {
            styleClass: "col-md-6",
            widgets: [{
              fullScreen: true,
              modalSize: 'sm',
              type: "chart",
              config: {
                options: top10MtdLossesOptions,
                data: top10MtdLossesData
              },
              title: "Top 10 MTD Losses"
            }]
          }]
        }, {
          columns: [{
            styleClass: "col-md-12",
            widgets: [$scope.top10YtdGainsGrid]
          }, {
            styleClass: "col-md-12",
            widgets: [$scope.top10YtdGainsDetailsLineChart]
          }]
        }, {
          columns: [{
            styleClass: "col-md-6",
            widgets: [$scope.etlJobsDoneGauge]
          }, {
            styleClass: "col-md-6",
            widgets: [$scope.etlJobsFailedGauge]
          }]
        }, {
          columns: [{
            styleClass: "col-md-12",
            widgets: [{
              fullScreen: true,
              modalSize: 'sm',
              type: 'treemap',
              title: 'Current Etl Job Execution Summary',
              config: {
                data: treemapData
              }
            }]
          }]
        }]
      };
      $scope.model = model;
    }

    // pnlByBookPieChart
    $scope.pnlByBookPieChartConfigOptions = buildPnlByBookChartOptions($scope);
    $scope.pnlByBookPieChartConfigData = pnlByBookData;
    $scope.pnlByBookPieChartDrillingDown = false;
    $scope.pnlByBookPieChart = {
      fullScreen: true,
      modalSize: 'sm',
      type: "chart",
      config: {
        options: $scope.pnlByBookPieChartConfigOptions,
        data: $scope.pnlByBookPieChartConfigData,
        drillDownEnabled: true,
        drillDownOptions: {
          "chart": {
            "type": "lineChart",
            "height": 500,
            margin: {
              right: 40
            },
            "useInteractiveGuideline": true,
            "dispatch": {},
            x: function(d) {
              return (new Date(d.date)).valueOf();
            },
            y: function(d) {
              return d.mtd;
            },
            "xAxis": {
              "axisLabel": "Date",
              tickFormat: function(d) {
                return d3.time.format('%x')(new Date(d))
              }
            },
            "yAxis": {
              "axisLabel": "PnL ($Millions)",
              "axisLabelDistance": 2
            },
            xScale: d3.time.scale(),
            "transitionDuration": 250
          },
          "title": {
            "enable": true,
            "text": 'By Sub-Book'
          }
        }
      },
      title: "PnL By Book"
    };

    // top10YtdGainsGrid
    $scope.top10YtdGainsGrid = {
      fullScreen: true,
      modalSize: 'lg',
      height: 500,
      type: "grid",
      config: {
        gridOptions: {
          data: top10YtdGainsData,
          columnDefs: top10YtdGainsColumnDefs,
          enableRowHeaderSelection: true,
          enableRowSelection: true,
          onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function(row) {

              var selectedRows = gridApi.selection.getSelectedRows();
              var tempData = [];

              if (selectedRows.length > 0) {
                for (var i in selectedRows) {
                  for (var j in fullTop10YtdGainsDetailsData) {
                    if (selectedRows[i].Strategy == fullTop10YtdGainsDetailsData[j].key) {
                      tempData.push(fullTop10YtdGainsDetailsData[j]);
                    }
                  }
                }
              } else {
                tempData = fullTop10YtdGainsDetailsData;
              }

              while ($scope.top10YtdGainsDetailsData.length > 0) {
                $scope.top10YtdGainsDetailsData.pop();
              }

              for (var i in tempData) {
                $scope.top10YtdGainsDetailsData.push(tempData[i]);
              }

            });
          }
        }
      },
      title: "Top 10 YTD Gains"
    };

    // top10YtdGainsDetailsLineChart
    var fullTop10YtdGainsDetailsData = angular.copy(top10YtdGainsDetailsData);
    $scope.top10YtdGainsDetailsData = top10YtdGainsDetailsData;
    $scope.top10YtdGainsDetailsLineChart = {
      fullScreen: true,
      modalSize: 'lg',
      height: 500,
      type: "chart",
      config: {
        options: {
          "chart": {
            "type": "lineChart",
            "height": 500,
            margin: {
              left: 100,
              right: 40
            },
            "useInteractiveGuideline": true,
            x: function(d) {
              return (new Date(d.date)).valueOf();
            },
            y: function(d) {
              return d.mtd;
            },
            "xAxis": {
              "axisLabel": "Date",
              tickFormat: function(d) {
                return d3.time.format('%b')(new Date(d))
              }
            },
            "yAxis": {
              "axisLabel": "MTD PnL",
              "axisLabelDistance": 2
            },
            xScale: d3.time.scale(),
            "transitionDuration": 250
          },
          "title": {
            "enable": true,
            "text": 'Top 10 YTD Gains Details'
          }
        },
        data: $scope.top10YtdGainsDetailsData
      },
      title: "Top 10 YTD Gains Details"
    };

    $scope.etlJobsDoneGauge = {
      fullScreen: true,
      modalSize: 'sm',
      type: "gauge",
      config: {
        title: 'ETL Jobs Done',
        value: 10,
        min: 0,
        max: 20
      },
      title: "ETL Jobs Done"
    };

    $scope.etlJobsFailedGauge = {
      fullScreen: true,
      modalSize: 'sm',
      type: "gauge",
      config: {
        title: 'ETL Jobs Failed',
        value: 2,
        min: 0,
        max: 10
      },
      title: "ETL Jobs Failed"
    };

    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.formats = ['MM-dd-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.$watch('dt', function(newValue, oldValue) {
      console.log(newValue);
      if (model) {
        // simulate cascading data change
        console.info('randomizing...')
        var mtdPnls = model.rows[0].columns[0].widgets[0].config.data;
        for (var i in mtdPnls) {
          mtdPnls[i].mtdPnl += (Math.random() * 2 - 1) * 5000000;
          if (mtdPnls[i].mtdPnl < 0) {
            mtdPnls[i].mtdPnl = 0;
          }
        }


        // gauge
        $scope.etlJobsDoneGauge.config.value += (Math.random() * 2 - 1) * 10;
        if ($scope.etlJobsDoneGauge.config.value < $scope.etlJobsDoneGauge.config.min) {
          $scope.etlJobsDoneGauge.config.value = $scope.etlJobsDoneGauge.config.min;
        } else if ($scope.etlJobsDoneGauge.config.value > $scope.etlJobsDoneGauge.config.max) {
          $scope.etlJobsDoneGauge.config.value = $scope.etlJobsDoneGauge.config.max;
        }

        $scope.etlJobsFailedGauge.config.value += (Math.random() * 2 - 1) * 5;
        if ($scope.etlJobsFailedGauge.config.value < $scope.etlJobsFailedGauge.config.min) {
          $scope.etlJobsFailedGauge.config.value = $scope.etlJobsFailedGauge.config.min;
        } else if ($scope.etlJobsFailedGauge.config.value > $scope.etlJobsFailedGauge.config.max) {
          $scope.etlJobsFailedGauge.config.value = $scope.etlJobsFailedGauge.config.max;
        }
        setTimeout(function() {
          $scope.$apply();
        }, 300);
      }
    });

    var name = 'dashboard';
    var model = localStorageService.get(name);
    if (!model) {
      var etlJobExecutionSummary = null;

      $http({
        method: 'GET',
        url: '/EtlJobExecutionSummary',
        // withCredentials: true,
        headers: {
          'Authorization': 'Basic RmFrZURQZXJtc181Yzk1NmVlZjU0Nzc0Y2JhYjEzYmUyOWY2YWQ2YWVkYzo='
        }
      }).success(function(data, status, headers, config) {
        console.info('get data success' + data);
        etlJobExecutionSummary = [{
          name: 'Done',
          value: data[0]['Done']
        }, {
          name: 'Waiting',
          value: data[0]['Waiting']
        }, {
          name: 'Late',
          value: data[0]['Late']
        }, {
          name: 'Failed',
          value: data[0]['Failed']
        }];
        buildModel(etlJobExecutionSummary);
      }).error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        console.warn('get data failed:', data, status);
      });

    } else {
      $scope.model = model;
    }

    $scope.name = name;

    $scope.collapsible = true;
    $scope.maximizable = true;

    $scope.$on('adfDashboardChanged', function(event, name, model) {
      // localStorageService.set(name, model);
      console.log('adfDashboardChanged');
    });

    $rootScope.$watch('containerClass', function(newVal, oldVal) {
      if (newVal && model != null) {
        console.log('container class changed');
        localStorageService.set('containerClass', newVal);
        location.reload();
      }
    });
  });
